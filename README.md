# Startup Generator Application

## Setting up your device to run flutter application

I assume you already downloaded and installed flutter Software Development Kit. Now to create a new project you need to run:

```bash
flutter create appname
```

After that open your flutter project in `Visual Studio Code` or `Android Studio` or any other text editor like `Vim` with coc.nvim extensions installed(coc_extension: coc-flutter)

Also try to plug your device in your USB port with `USB Debugging` enabled. Or if you already have emulator installed you can run below command:

```bash
~/Android/Sdk/emulator/emulator -avd Pixel_XL_API_R -netdelay none -netspeed full
```

To know which image has been downloaded you can run below command:

```bash
~/Android/Sdk/emulator/emulator -list-avds
```

Here I am using terminal to run flutter application. So inside terminal I need to run:

```bash
flutter doctor
```

to check if there is any issue or not. If there is any issue it will highlight it, you can run this command with -v flag for `verbose` information.

If all goes well you will see the application is running on your android device.

## Start coding

Open `main.dart` file from the `lib` folder and replace with following codes:

```dart
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Welcome to Flutter',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Welcome to Flutter'),
        ),
        body: Center(
          child: Text('Hello World'),
        ),
      ),
    );
  }
}
```

Now Restart your app by pressing `R` key in your terminal.

![Hello world!](https://gitlab.com/techcetblog/flutter-startup-name-generator/-/raw/part1/assets/images/ss_01.jpg)

So far so good, now we need to install `english_words` package, so to that we need to edit `pubspec.yaml` file and add following:

```yaml
dependencies:
flutter:
sdk: flutter
cupertino_icons: ^0.1.
english_words: ^3.1.0
```

And then go to terminal stop the debugging of flutter application by pressing `<Control-C>` and run following command:

```bash
pub get
# or
flutter pub get
```

Then import it inside the lib/main.dart file:

```dart
import 'package:flutter/material.dart';
import 'package:english_words/english_words.dart';
```

Now we need to create a stateful widget, so create a minimal state class. Add the following to the bottom of main.dart:

```dart
class RandomWordsState extends State<RandomWords> {
    // TODO Add build() method
}
```

Add the stateful RandomWords widget to main.dart. The RandomWords widget does little else beside creating its State class:

```dart
class RandomWords extends StatefulWidget {
    @override
    RandomWordsState createState() => RandomWordsState();
}
```

Add the build() method to RandomWordsState:

```dart
class RandomWordsState extends State<RandomWords> {
    @override
    Widget build(BuildContext context) {
        final wordPair = WordPair.random();
        return Text(wordPair.asPascalCase);
    }
}
```

Remove the word generation code from MyApp by making the changes shown in:

```dart
class MyApp extends StatelessWidget {
    @override
    Widget build(BuildContext context) {
        final wordPair = WordPair.random();
        return MaterialApp(
            title: 'Welcome to Flutter',
            home: Scaffold(
                title: Text('Welcome to Flutter'),
            ),
            body: Center(
                child: RandomWords(),
            ),
        ),
    );
}
```

Restart the app. The app should behave as before, displaying a word pairing each time you hot reload or save the app.

![Random Words](https://gitlab.com/techcetblog/flutter-startup-name-generator/-/raw/part1/assets/images/ss_02.jpg)

## Create an infinite scrolling ListView

1. Add a \_suggestions list to the RandomWordsState class for saving suggested word pairings. Also, add a \_biggerFont variable for making the font size larger.

```dart
class RandomWordsState extends State<RandomWords> {
    final _suggestions = <WordPair>[];
    final _biggerFont = const TextStyle(fontSize: 18.0);
    // ···
}
```

2. Add a \_buildSuggestions() function to the RandomWordsState class:

```dart
Widget _buildSuggestions() {
    return ListView.builder(
        padding: const EdgeInsets.all(16.0),
        itemBuilder: /*1*/ (context, i) {
            if (i.isOdd) return Divider(); /*2*/

            final index = i ~/ 2; /*3*/
            if (index >= _suggestions.length) {
                _suggestions.addAll(generateWordPairs().take(10)); /*4*/
            }
            return _buildRow(_suggestions[index]);
    });
}
```

3. Add a \_buildRow() function to RandomWordsState:

```dart
Widget _buildRow(WordPair pair) {
  return ListTile(
    title: Text(
      pair.asPascalCase,
      style: _biggerFont,
    ),
  );
  }
```

4. In the RandomWordsState class, update the build() method to use \_buildSuggestions(), rather than directly calling the word generation library. (Scaffold implements the basic Material Design visual layout.) Replace the method body with the highlighted code:

```dart
@override
Widget build(BuildContext context) {
  return Scaffold(
    appBar: AppBar(
      title: Text('Startup Name Generator'),
    ),
    body: _buildSuggestions(),
  );
  }
```

5. In the MyApp class, update the build() method by changing the title, and changing the home to be a RandomWords widget:

```dart
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Startup Name Generator',
      home: RandomWords(),
    );
  }
}
```

6. Restart the app. You should see a list of word pairings no matter how far you scroll.

![Random Words](https://gitlab.com/techcetblog/flutter-startup-name-generator/-/raw/part1/assets/images/ss_03.jpg)
